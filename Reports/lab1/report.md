                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           

# lab1 实验报告

学号 202108010423 姓名 韦华喜

## 实验要求

根据`cminux-f`的词法补全`lexical_analyer.l`文件，完成词法分析器，能够输出识别出的`token`，`type` ,`line`(刚出现的行数)，`pos_start`(该行开始位置)，`pos_end`(结束的位置,不包含)

文本输入：

```c++
int a;
```

则识别结果应为：

```c++
int     280     1       2       5
a       285     1       6       7
;       270     1       7       8
```

对于部分token，我们只需要进行过滤，即只需被识别，但是不应该被输出到分析结果中。因为这些token对程序运行不起到任何作用。



## 实验难点

1. 实验环境的搭建以及配置，比如ubuntu20.04的安装以及flex编译环境的配置等。
2. 学习并掌握cminus-f语法，提供其词法的正则表达式。
3. 学会利用FLEX并使用正则表达式来编写正确的词法分析器程序，能够识别各种词语



## 实验设计

1.实验要求能够识别出所有的输入token，但并不是所有的内容都需要识别。具体需要识别的token在 **lexical_analyzer.h** 定义了，打开**lexical_analyzer.h** 并查看具体要识别的token以及其对应的字符和含义，符号的编号和具体内容如下：

先通过示例文件tokens，并参考网上资料，找到各Token符号对应的字符

```c++
//运算
ADD = 259, 	/* 加号：+ */
SUB = 260, 	/* 减号：- */
MUL = 261, 	/* 乘号：* */
DIV = 262, 	/* 除法：/ */
LT = 263, 	/* 小于：< */
LTE = 264, 	/* 小于等于：<= */
GT = 265, 	/* 大于：> */
GTE = 266, 	/* 大于等于：>= */
EQ = 267, 	/* 相等：== */
NEQ = 268, 	/* 不相等：!= */
ASSIN = 269,/* 单个等于号：= */
    
//符号
SEMICOLON = 270,	/* 分号：; */
COMMA = 271, 		/* 逗号：, */
LPARENTHESE = 272, 	/* 左括号：( */
RPARENTHESE = 273, 	/* 右括号：) */
LBRACKET = 274, 	/* 左中括号：[ */
RBRACKET = 275, 	/* 右中括号：] */
LBRACE = 276, 		/* 左大括号：{ */
RBRACE = 277, 		/* 右大括号：} */
    
//关键字
ELSE = 278, 	/* else */
IF = 279, 		/* if */
INT = 280, 		/* int */
FLOAT = 281, 	/* float */
RETURN = 282, 	/* return */
VOID = 283, 	/* void */
WHILE = 284,	/* while */
    
//ID和NUM
IDENTIFIER = 285,	/* 变量名，例如a,b */
INTEGER = 286, 		/* 整数，例如1，2 */
FLOATPOINT = 287,	/* 浮点数，例如1.1,1.2 */
ARRAY = 288,		/* 数组，例如[] */
LETTER = 289,		/* 单个字母，例如a,z */	
    
//others
EOL = 290,			/* 换行符，\n或\0 */	
COMMENT = 291,		/* 注释 */
BLANK = 292,		/* 空格 */
ERROR = 258			/* 错误 */
```



#### 识别到词法单元的动作

- 运算符，符号，关键字，ID和NUM类型的词法单元在识别后，**需要确定出现的行数，开始位置，结束的位置。**
- **出现的行数可以在识别到换行符时lines++实现，开始的位置为上一个识别的词法单元结束的位置，结束的位置为开始位置加上词素长度。**

**最后返回token值结束完成一个词素的识别**。因此识别到词法单元的动作如下：

```c
RE {pos_start=pos_end;pos_end=pos_start+strlen(yytext);return token}
```



#### 注释的正则表达式

注释的正则表达式需要注意，因为匹配的原则是最长匹配，但是如果有多个注释，中间的代码会被当作注释的内容匹配：

```c
/*comments*/
int a = 1
/*comments again*/


因此在进行匹配时，/\*和\*/之间不能有*/，即中间的连续字符可以划分为两种情况：

没有出现*：可以表示为[^*]
*后加除/以外的任何字符：\*+[^/]

将注释的正则表达式写为"/\*"(\[^\*] | \\\*+\[^\\])+"\*/"，但是这样写出现了一个问题，\*\*\*被匹配到一起了，导致\*和/分离。修改第二种情况为\\\*\[^*/]，将\*\*\*/这种情况放到最后来解决这个问题。最终修改后注释的正则表达式如下：
```



最终修改后注释的正则表达式如下：

```C
"/*"([^*]|\*+[^*/])*\*+"/"
```



| token       | type | 正则表达式    |
| ----------- | ---- | ------------- |
| ADD         | 259  | +             |
| SUB         | 260  | -             |
| MUL         | 261  | *             |
| DIV         | 262  | /             |
| LT          | 263  | <             |
| LTE         | 264  | <=            |
| GT          | 265  | >             |
| GTE         | 266  | `>=`          |
| EQ          | 267  | ==            |
| NEQ         | 268  | !=            |
| ASSIN       | 269  | =             |
| SEMICOLON   | 270  | ;             |
| COMMA       | 271  | ,             |
| LPARENTHESE | 272  | (             |
| RPARENTHESE | 273  | )             |
| LBRACKET    | 274  | [             |
| RBRACKET    | 275  | ]             |
| LBRACE      | 276  | {             |
| RBRACE      | 277  | }             |
| ELSE        | 278  | else          |
| IF          | 279  | if            |
| INT         | 280  | int           |
| FLOAT       | 281  | float         |
| RETURN      | 282  | return        |
| VOID        | 283  | void          |
| WHILE       | 284  | while         |
| IDENTIFIER  | 285  | [a-zA-Z]+     |
| INTEGER     | 286  | [0-9]+        |
| FLOATPOINT  | 287  | [0-9]+.[0-9]+ |
| ARRAY       | 288  | `\[\]`        |
| LETTER      | 289  | [a-zA-Z]      |
| EOL         | 290  | [\n]+         |
| COMMENT     | 291  | `/*([^*]      |
| BLANK       | 292  | [ \f\n\r\t\v] |
| ERROR       | 258  | .             |



#### 写出指定模式匹配时对应的动作

实验文档的基础知识中给出了如何写一个简单的单词数量统计的程序

```c++
%{
//在%{和%}中的代码会被原样照抄到生成的lex.yy.c文件的开头，您可以在这里书写声明与定义
#include <string.h>
int chars = 0;
int words = 0;
%}

%%
 /*你可以在这里使用你熟悉的正则表达式来编写模式*/
 /*你可以用C代码来指定模式匹配时对应的动作*/
 /*yytext指针指向本次匹配的输入文本*/
 /*左部分（[a-zA-Z]+）为要匹配的正则表达式，
 	右部分（{ chars += strlen(yytext);words++;}）为匹配到该正则表达式后执行的动作*/
[a-zA-Z]+ { chars += strlen(yytext);words++;}


. {}
 /*对其他所有字符，不做处理，继续执行*/

%%

int main(int argc, char **argv){
    //yylex()是flex提供的词法分析例程，默认读取stdin      
    yylex();                                                               
    printf("look, I find %d words of %d chars\n", words, chars);
    return 0;
}
```

在第二部分要求我们使用熟悉的正则表达式来编写模式，那么可以仿照上面的程序来编写出对应的动作



**yytext指针指向本次匹配的输入文本**，我们在左部分写要匹配的正则表达式，右部分为匹配到该正则表达式后执行的动作。 

除了一些特殊的字符要在`analyzer`函数中实现外，其余部分的实现都是差不多的。题目要求能够输出识别出的**token，type ,line(刚出现的行数)，pos_start(该行开始位置)，pos_end(结束的位置,不包含)** 。

- 首先将开始位置和结束位置设置成一样，代表当前识别的字符从上一识别完的字符的末尾开始。
- 接着设置**`pos_end+=strlen(yytext) `，`strlen(yytext)`**为这次识别到的长度，然后**返回识别出的token即可。**

代码如下：

```c
 /****请在此补全所有flex的模式与动作  start******/
 //STUDENT TO DO
 /******** 运算 ********/
\+ {pos_start=pos_end;pos_end+=strlen(yytext);return ADD;}
\- {pos_start=pos_end;pos_end+=strlen(yytext);return SUB;}
\* {pos_start=pos_end;pos_end+=strlen(yytext);return MUL;}
\/ {pos_start=pos_end;pos_end+=strlen(yytext);return DIV;}
\< {pos_start=pos_end;pos_end+=strlen(yytext);return LT;}
\<\= {pos_start=pos_end;pos_end+=strlen(yytext);return LTE;}
\> {pos_start=pos_end;pos_end+=strlen(yytext);return GT;}
\>\= {pos_start=pos_end;pos_end+=strlen(yytext);return GTE;}
\=\= {pos_start=pos_end;pos_end+=strlen(yytext);return EQ;}
\!\= {pos_start=pos_end;pos_end+=strlen(yytext);return NEQ;}
\= {pos_start=pos_end;pos_end+=strlen(yytext);return ASSIN;}

/******** 符号 ********/
\; {pos_start=pos_end;pos_end+=strlen(yytext);return SEMICOLON;}
\, {pos_start=pos_end;pos_end+=strlen(yytext);return COMMA;}
\( {pos_start=pos_end;pos_end+=strlen(yytext);return LPARENTHESE;}
\) {pos_start=pos_end;pos_end+=strlen(yytext);return RPARENTHESE;}
\[ {pos_start=pos_end;pos_end+=strlen(yytext);return LBRACKET;}
\] {pos_start=pos_end;pos_end+=strlen(yytext);return RBRACKET;}
\{ {pos_start=pos_end;pos_end+=strlen(yytext);return LBRACE;}
\} {pos_start=pos_end;pos_end+=strlen(yytext);return RBRACE;}

/******** 关键字 ********/
else {pos_start=pos_end;pos_end+=strlen(yytext);return ELSE;}
if {pos_start=pos_end;pos_end+=strlen(yytext);return IF;}
int {pos_start=pos_end;pos_end+=strlen(yytext);return INT;}
float {pos_start=pos_end;pos_end+=strlen(yytext);return FLOAT;}
return {pos_start=pos_end;pos_end+=strlen(yytext);return RETURN;}
void {pos_start=pos_end;pos_end+=strlen(yytext);return VOID;}
while {pos_start=pos_end;pos_end+=strlen(yytext);return WHILE;}

/******** ID和NUM ********/
[a-zA-Z]+ {pos_start=pos_end;pos_end+=pos_start+strlen(yytext);return IDENTIFIER;}
[0-9]+ {pos_start=pos_end;pos_end+=pos_start+strlen(yytext);return INTEGER;}
[0-9]+\.[0-9]+ {pos_start=pos_end;pos_end+=pos_start+strlen(yytext);return FLOATPOINT;}
\[\] {pos_start=pos_end;pos_end+=strlen(yytext);return ARRAY;}
[a-zA-Z] {pos_start=pos_end;pos_end+=strlen(yytext);return LETTER;}

/******** others ********/
[\n]+ {pos_start = 1;pos_end = 1;lines+=strlen(yytext);return EOL;}
\/\*([^\*]|(\*)*[^\*\/])*(\*)*\*\/ {return COMMENT;}
[ \f\n\r\t\v] {pos_start = pos_end;pos_end+=strlen(yytext);return BLANK;}
. {return ERROR;}
 /****请在此补全所有flex的模式与动作  end******/
```



#### 3.识别到特殊词法单元的动作

对于注释，空格，换行符，只需要进行识别，不需要输出到分析结果中。这些token与程序运行无关。

- **其中空格对接下来的词法单元无影响**，与一般词法单元的动作相同；
- **识别到换行符需要将lines++**，并将开始位置置1；
- 识别到注释时，由于注释中内容全部都与程序无关，对于注释COMMENT，
- 先获yytext的长度为len,然后通过循环判断注释中是否存在换行符，即判断yytext[i]是否为换行符，如果是换行\n的话，将pos_start和pos_end设置为1，lines加1；否则pos_end++;当循环结束则break;

因此在识别到这些词法单元时，处理如下：

```c
    int len;
	while(token = yylex()){ //yylex()是flex提供的词法分析例程，默认读取stdin 
        switch(token){
            case COMMENT:/*注释*/
                len = strlen(yytext);	 /*yytext指针指向本次匹配的输入文本*/
                for(int i=0;i<len;i++)
                {
                    if(yytext[i]=='\n')
                    {
                        lines++;
                        pos_end = 1;	//pos_start由pos_end得到，这里就不需要置1了
                    }
                    else pos_end++;
                }
                break;
            case BLANK:/*空格*/
                break;
            case EOL:/*换行符*/
                lines++;
                pos_end = 1;
                break;
            case ERROR:
                printf("[ERR]: unable to analysize %s at %d line, from %d to %d\n", yytext, lines, pos_start, pos_end);
            default :
                if (token == ERROR){
                    sprintf(token_stream[index].text, "[ERR]: unable to analysize %s at %d line, from %d to %d", yytext, lines, pos_start, pos_end);
                } else {
                    strcpy(token_stream[index].text, yytext);
                }
                token_stream[index].token = token;
                token_stream[index].lines = lines;
                token_stream[index].pos_start = pos_start;
                token_stream[index].pos_end = pos_end;
                index++;
                if (index >= MAX_NUM_TOKEN_NODE){
                    printf("%s has too many tokens (> %d)", input_file, MAX_NUM_TOKEN_NODE);
                    exit(1);
                }
        }
    }
```









## 实验结果验证

1.创建build文件夹，配置编译环境, 执行指令`make lexer`运行代码，开始编译，如图所示，编译成功

![image-20231112230010050](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231112230010050.png)





2.执行指令python3 ./tests/lab1/test_lexer.py来查看是否通过6个测试样例 。可以看到，通过了6个测试样例

![image-20231112230834272](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231112230834272.png)



3.验证结果正确性。输入`diff ./tests/lab1/token ./tests/lab1/TA_token` 将自己的生成结果和助教提供的`TA_token`进行比较。如果结果完全正确，则没有任何输出结果。如果有不一致，则会汇报具体哪个文件哪部分不一致。如图所示，是正确的。



![image-20231112230957847](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231112230957847.png)



4.请提供部分自己的测试样例

自己测试样例：

![image-20231117213911658](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231117213911658.png)

使用指令`./build/lexer ./tests/lab1/testcase/mytest.cminus out`,得到如下输出：

![image-20231117213749133](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231117213749133.png)

正确。



## 实验反馈

- 学习了词法分析器生成工具Flex的使用
- 对于正则表达式的各种写法有了更深的理解。比如对于数字的正则表达式，就有`[0-9]+`和`\d`等几种不同的写法。
- 一些比较复杂但有有规律的正则表达式，使用语法糖可以有效的简化结构，比如英文字符，正则表达式为a|b|c|d……|z，使用语法糖可以[a-z]直接表示。
- 补充完了一个识别token的程序，从而实现对输入的词法进行识别和分析的功能，并能准确给出token所在行，开始位置和结束位置、

