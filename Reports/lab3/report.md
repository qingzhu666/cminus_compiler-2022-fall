

# lab3 实验报告

##### 学号 202108010423 姓名 韦华喜

###  主要工作

1. 第一部分: 了解LLVM IR。通过clang生成的.ll，了解LLVM IR与c代码的对应关系。完成1.3
2. 第二部分: 了解LightIR。通过助教提供的c++例子，了解LightIR的c++接口及实现。完成2.3
3. 第三部分: 理解Visitor Pattern。
4. 实验报告：在report.md中**回答3个问题**。



## 问题1: cpp与.ll的对应



**请描述你的cpp代码片段和.ll的每个BasicBlock的对应关系。描述中请附上两者代码。**

#####  

**Terminator Instructions**

- `ret`指令用于将控制流（以及可选的值）从函数返回给调用者。
- `br`指令用于使控制流转移到当前功能中的另一个基本块。

 **Standard binary operators**

`add`指令返回其两个`i32`类型的操作数之和

- 格式：
  - `<result> = add <type> <op1>, <op2>`
  - `<result> = fadd <type> <op1>, <op2>`



**Memory operators**

- `alloca`指令在当前执行函数的堆栈帧上分配内存，当该函数返回其调用者时将自动释放该内存。 
- `load`指令用于从内存中读取。
- `store`指令用于写入内存
- `zext`指令将其操作数**零**扩展为`type2`类型。格式：`<result> = zext <type> <value> to <type2>`
- `fptosi`指令将浮点值转换为`type2`（整数）类型。格式：`<result> = fptosi <type> <value> to <type2>`
- `sitofp`指令将有符号整数转换为`type2`（浮点数）类型。格式：`<result> = sitofp <type> <value> to <type2>`



 **Other operators**

`icmp`指令根据两个整数的比较返回布尔值，`fcmp`指令根据两个浮点数的比较返回布尔值。

`call`指令用于使控制流转移到指定的函数，其传入参数绑定到指定的值。 

`getelementptr`指令用于获取数组结构的元素的地址。 它仅执行地址计算，并且不访问内存。

- 格式：`<result> = getelementptr <type>, <type>* <ptrval> [, <type> <idx>]`
- 参数解释：第一个参数是计算基础类型，第二第三个参数表示索引开始的指针类型及指针，`[]`表示可重复参数，里面表示的数组索引的偏移类型及偏移值。（Question：思考指针类型为`[10 x i32]`指针和`i32`指针`getelementptr`用法的不同，并给出解释，实验结束后回答两者使用情况的区别）
- 例子：
  - `%2 = getelementptr [10 x i32], [10 x i32]* %1, i32 0, i32 %0`
  - `%2 = getelementptr i32, i32* %1 i32 %0`



根据 `1.2 gcd例子: 利用clang生成的.ll` 进行学习

**输入`clang -S -emit-llvm gcd_array.c`指令得到对应的`gcd_array.ll`文件.：**

![image-20231129151804048](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129151804048.png)

**通过`lli gcd_array.ll; echo $?`指令，测试`gcd_array.ll`执行结果的正确性。**其中，

- `lli`会运行`*.ll`文件
- `$?`的内容是上一条命令所返回的结果，而`echo $?`可以将其输出到终端中

查看 gcd_array.ll 文件，并输入`lli gcd_array.ll; echo $?`指令，验证 gcd_array.ll 的正确性：

![image-20231129151839258](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129151839258.png)

可以得出其运行后，输出结果也为18。 因此，可以验证 `gcd_array.ll` 文件正确地对应了 `gcd_array.c` 文件。



##### 补充`assign_hand.ll`

查看 `assign.c` 文件

```c
int main(){
  int a[10];
  a[0] = 10;
  a[1] = a[0] * 2;
  return a[1];
}
```

很明显运行结果返回20，下面根据上面的测试样例和精简的IR Reference基本手册，补充补充` assign_hand.ll`

```c
; ModuleID = 'assign.c'
source_filename = "assign.c"
; target的开始
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"
;target的结束

; 全局main函数的定义
define dso_local i32 @main() #0{
	;第一个基本块的开始
	;语句：int a[10];创建空间，并返回指针
	%1 = alloca [10 x i32]	
	
	;语句：a[0] = 10;获取a[0]指针
	%2 = getelementptr inbounds [10 x i32], [10 x i32]* %1, i64 0, i64 0
        
	; 将10存入a[0]中
	store i32 10, i32* %2
	
	;语句：a[1] = a[0] * 2;
    
	%3 = load i32, i32* %2	; 取出a[0],存到%3中
	%4 = mul i32 %3, 2		; 将值乘以2，存在%4中
	%5 = getelementptr inbounds [10 x i32], [10 x i32]* %1, i64 0, i64 1	;获取a[1]指针
	store i32 %4, i32* %5	; 写入a[1]中
	
	; 语句：return a[1]
	ret i32 %4				; 将a[0]返回
}
```

结果验证：

运行`assign.c` ，输入命令：`gcc -o assign assign.c `和`./assign;echo $?`，结果如下：

![image-20231129152340445](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129152340445.png)



运行刚才补充的`assign_hand.ll `，输入命令`lli assign_hand.ll; echo $?` ，运行结果如下：

![image-20231129152509666](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129152509666.png)



可以看到，结果一样，都为20，说明补充正确。`assign_hand.ll`文件正确地对应了 `assign.c` 文件。

##### 补充`fun_hand.ll`

查看 `fun.c `文件

```c
int callee(int a){
  return 2 * a;
}
int main(){
  return callee(110);
}
```



很明显运行结果返回220，下面根据上面的测试样例和精简的IR Reference基本手册，补充补充` fun_hand.ll`

```c
; ModuleID = 'fun.c'
source_filename = "fun.c"
; target的开始
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"
;target的结束
    
; 语句：int callee(int a)。callee 函数的定义
define dso_local i32 @callee(i32) #0{
    ; 语句：return 2 * a
    %2 = mul i32 %0, 2		; 将参数%0乘以2存入%2中
	ret i32 %2				; 返回%2
}

; main 函数的定义
define dso_local i32 @main() #0{
    ; 语句：return callee(110);
	%1 = call i32 @callee(i32 110)	; 调用函数callee，并将结果存到%1中
	ret i32 %1					; 返回%1
}
```



结果验证：

运行`fun.c` ，输入命令：`gcc -o fun fun.c `和`./fun;echo $?`，结果如下：

![image-20231129152737612](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129152737612.png)

运行刚才补充的`fun_hand.ll `，输入命令`lli fun_hand.ll; echo $?` ，运行结果如下：

![image-20231129152949453](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129152949453.png)



- 第一点是对callee函数定义时`define dso_local i32 @callee(i32)`传入的参数如果只有一个int型，可以直接传入i32，默认传入参数为%0，如果写的是`i32 %0`会报错，初步认为应该和llvm版本有关。
- 第二点就是` %2 = mul i32 %0, 2`这一条语句，为什么不能用%1进行赋值，会报错
- 程序也会建议使用%2赋值



##### 补充`if_hand.ll`

查看 `fun.c `文件

```c
int main(){
  float a = 5.555;
  if(a > 1)
    return 233;
  return 0;
}
```



很明显运行结果返回233，下面根据上面的测试样例和精简的IR Reference基本手册，补充补充` fun_hand.ll`



```c
; ModuleID = 'if.c'
source_filename = "if.c"
; target的开始
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"
;target的结束

; main 函数的定义
define dso_local i32 @main() #0
{
	;语句：; float a = 5.555;
	%1 = alloca float      ;为a开辟栈空间
	store float 0x40163851E0000000, float* %1  ;将浮点数5.555写入a指向的内存空间
	
	; 语句：if(a > 1)
	%2 = load float, float* %1		; 取出%1所指向的空间中的值赋给%2
	%3 = fcmp ugt float %2, 1.000	; 将其和1进行比较，返回结果到%3中，是布尔型
	br i1 %3, label %lebel_1, label %lebel_2	; 如果%3为1，则跳转到%4，否则跳转到%5

lebel_1:	; 语句：return 233;
	ret i32 233		; 返回233

lebel_2:	; 语句：return 0;
	ret i32 0		; 返回0
}
```

结果验证：

运行`if.c` ，输入命令：`gcc -o if if.c `和`./if;echo $?`，结果如下：

![image-20231129153744948](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129153744948.png)

运行刚才补充的`fun_hand.ll `，输入命令`lli fun_hand.ll; echo $?` ，运行结果如下：

![image-20231129153843413](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129153843413.png)



##### 补充`while.ll`

查看 `while.c `文件

```c
int main(){
  int a;
  int i;
  a = 10;
  i = 0;
  while(i < 10){
    i = i + 1;
    a = a + i;
  }
  return a;
}
```



很明显运行结果返回65，下面根据上面的测试样例和精简的IR Reference手册，补充补充 fun_hand.ll

```
; ModuleID = 'if.c'
source_filename = "if.c"
; target的开始
target datalayout = "e-m:e-p270:32:32-p271:32:32-p272:64:64-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-pc-linux-gnu"
;target的结束

; main 函数的定义
define dso_local i32 @main() #0{
	;为变量a和i开辟栈空间
	%1=alloca i32
	%2=alloca i32
	;将10和0写入a和i指向的内存空间
	store i32 10,i32* %1
	store i32 0,i32* %2
	;跳到while条件判断中
	br label %label_1

;i < 10
label_1:
	;取出i的值赋给%3
	%3=load i32, i32* %2
	;将i和10进行比较，得到的结果为布尔型赋给%4
	%4 = icmp ult i32 %3, 10
	;如果%4为1跳到label_2，否则跳到%label_3
	br i1 %4,label %label_2,label %label_3

;循环体的内容	
label_2:
	;i=i+1
	%5=add i32 %3 , 1
	store i32 %5,i32* %2
	;a=a+1,先取出a的值再相加再存回a里
	%6=load i32,i32* %1
	%7=add i32 %6,%5
	store i32 %7,i32* %1
	;跳回条件判断
	br label %label_1
;return a
label_3:
	;取出a的值
	%8=load i32,i32* %1
	;返回a
	ret i32 %8
}
```



结果验证：

运行`while.c` ，输入命令：`gcc -o while while.c `和`./while;echo $?`，结果如下：

![image-20231129154111697](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129154111697.png)

运行刚才补充的`while_hand.ll `，输入命令`lli while_hand.ll; echo $?` ，运行结果如下：

![image-20231129154204001](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231129154204001.png)



##### 补充`assign_generator.cpp`

```c
int main(){
  int a[10];
  a[0] = 10;
  a[1] = a[0] * 2;
  return a[1];

}
```

首先查看样例`gcd_array_generator.cpp`

按照`gcd_array_generator.cpp`来补充`assign_generator.cpp`

```c++
#include "BasicBlock.h"
#include "Constant.h"
#include "Function.h"
#include "IRBuilder.h"
#include "Module.h"
#include "Type.h"

#include <iostream>
#include <memory>

#ifdef DEBUG  // 用于调试信息,大家可以在编译过程中通过" -DDEBUG"来开启这一选项
#define DEBUG_OUTPUT std::cout << __LINE__ << std::endl;  // 输出行号的简单示例
#else
#define DEBUG_OUTPUT
#endif

#define CONST_INT(num) \
    ConstantInt::get(num, module)

#define CONST_FP(num) \
    ConstantFP::get(num, module) // 得到常数值的表示,方便后面多次用到
    
int main(){
	auto module = new Module("Cminus code");  // module name是什么无关紧要
	auto builder = new IRBuilder(nullptr, module);      // 创建IRBuilder,该类提供了独立的接口创建各种 IR 指令
	Type* Int32Type = Type::get_int32_type(module);

	// main函数
	auto mainFun = Function::create(FunctionType::get(Int32Type, {}),   /* 创建 main 函数 */	
	auto bb = BasicBlock::create(module, "entry", mainFun); 
    builder->set_insert_point(bb);  
    
    auto retAlloca = builder->create_alloca(Int32Type);  //分配一个保存返回值的内容
    
    auto *arrayType = ArrayType::get(Int32Type, 10);  //数组类型，参数依次是数组元素的类型Int32Type,数组元素个数10
    auto a=builder->create_alloca(arrayType);  //数组a[10]

    auto a0GEP = builder->create_gep(a, {CONST_INT(0), CONST_INT(0)}); //获取a[0]的地址
    builder->create_store(CONST_INT(10), a0GEP);  //将整数10写入a[0]的地址
    auto a0Load = builder->create_load(a0GEP);  //读取a[0]
    auto m=builder->create_imul(a0Load,CONST_INT(2)); //a[0]*2

    auto a1GEP = builder->create_gep(a, {CONST_INT(0), CONST_INT(1)});  //获取a[1]的地址
    builder->create_store(m, a1GEP); //将a[0]*2写入a[1]的地址
    auto a1Load = builder->create_load(a1GEP); //获取a[1]的值

    builder->create_store(a1Load, retAlloca); //将a[1]的值写入retAlloca地址中
    auto retLoad=builder->create_load(retAlloca); //读取retAlloca地址的值
    builder->create_ret(retLoad);  //创建ret指令，即返回

    std::cout << module->print();
    delete module;
    return 0;   
}
```



结果验证：

输入指令`make`，编译通过后输入指令`./stu_assign_generator>assign.ll`：

![image-20231206001939829](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206001939829.png)

`assign.ll`结果如下:

```c
define i32 @main() {
label_entry:
  %op0 = alloca i32
  %op1 = alloca [10 x i32]
  %op2 = getelementptr [10 x i32], [10 x i32]* %op1, i32 0, i32 0
  store i32 10, i32* %op2
  %op3 = load i32, i32* %op2
  %op4 = mul i32 %op3, 2
  %op5 = getelementptr [10 x i32], [10 x i32]* %op1, i32 0, i32 1
  store i32 %op4, i32* %op5
  %op6 = load i32, i32* %op5
  store i32 %op6, i32* %op0
  %op7 = load i32, i32* %op0
  ret i32 %op7
}
```

![image-20231206103150946](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103150946.png)

输入 `lli assign.ll; echo $?`指令进行测试，结果如下：

![image-20231206002004357](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206002004357.png)

**输出结果为 20，结果正确。**

只有一个`basicblock`,对应关系为：

`auto bb = BasicBlock::create(module, "entry", mainFun);`创建名为`entry`的基本块，并将其存到变量`bb`中，对应.ll文件中的`label_entry`块







##### 补充`fun_generator.cpp`

`fun_generator.cpp `代码如下：

```c++
#include "BasicBlock.h"
#include "Constant.h"
#include "Function.h"
#include "IRBuilder.h"
#include "Module.h"
#include "Type.h"

#include <iostream>
#include <memory>

#ifdef DEBUG  // 用于调试信息,大家可以在编译过程中通过" -DDEBUG"来开启这一选项
#define DEBUG_OUTPUT std::cout << __LINE__ << std::endl;  // 输出行号的简单示例
#else
#define DEBUG_OUTPUT
#endif

#define CONST_INT(num) \
    ConstantInt::get(num, module)

#define CONST_FP(num) \
    ConstantFP::get(num, module) // 得到常数值的表示,方便后面多次用到
int main() {
  auto module = new Module("fun code");
  auto builder = new IRBuilder(nullptr, module); //该类提供了独立的接口创建各种 IR 指令
  Type *Int32Type = Type::get_int32_type(module);
  
  //函数callee
  std::vector<Type *> Ints(1, Int32Type); // 函数参数类型的vector
  auto calleeFunTy = FunctionType::get(Int32Type, Ints); //通过返回值类型与参数类型列表得到函数类型
  auto calleeFun = Function::create(calleeFunTy,"callee", module);   // 由函数类型得到函数
  
  auto bb = BasicBlock::create(module, "entry", calleeFun);
  builder->set_insert_point(bb);                 // 一个BB的开始,将当前插入指令点的位置设在bb

  auto aAlloca = builder->create_alloca(Int32Type);  // 在内存中分配参数a的位置
 
  std::vector<Value *> args;  // 获取函数的形参,通过Function中的iterator
  for (auto arg = calleeFun->arg_begin(); arg != calleeFun->arg_end(); arg++) {
    args.push_back(*arg);   // * 号运算符是从迭代器中取出迭代器当前指向的元素
  }
  builder->create_store(args[0], aAlloca);  // 将参数a store下来
  auto aLoad=builder->create_load(aAlloca); //获取参数a的值
  auto m=builder->create_imul(aLoad,CONST_INT(2)); //a*2
  
  auto retAlloca = builder->create_alloca(Int32Type);  //分配一个保存返回值的内容
  builder->create_store(m, retAlloca); //将m的值写入retAlloca地址中
  auto retLoad=builder->create_load(retAlloca); //读取retAlloca地址的值
  builder->create_ret(retLoad);  //创建ret指令，即返回

  // main函数
  auto mainFun = Function::create(FunctionType::get(Int32Type, {}),"main", module);
  bb = BasicBlock::create(module, "entry", mainFun); //创建BasicBlock
  builder->set_insert_point(bb);  //将当前插入指令点的位置设在bb
  
  auto cs= builder->create_alloca(Int32Type); // 在内存中分配参数cs的位置
  builder->create_store(CONST_INT(110), cs);  //将整数10写入cs的地址
  auto csLoad = builder->create_load(cs);  //读取cs
  auto call = builder->create_call(calleeFun, {csLoad}); //创建call指令
  builder->create_ret(call); //创建ret指令,返回

  std::cout << module->print();
  delete module;
  return 0;
}
```



结果验证：

输入指令`make`，编译通过后输入指令`./stu_fun_generator >fun.ll`：

![image-20231206103216916](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103216916.png)

内容如下：

```c
define i32 @callee(i32 %arg0) {
label_entry:
  %op1 = alloca i32
  store i32 %arg0, i32* %op1
  %op2 = load i32, i32* %op1
  %op3 = mul i32 %op2, 2
  %op4 = alloca i32
  store i32 %op3, i32* %op4
  %op5 = load i32, i32* %op4
  ret i32 %op5
}
define i32 @main() {
label_entry:
  %op0 = alloca i32
  store i32 110, i32* %op0
  %op1 = load i32, i32* %op0
  %op2 = call i32 @callee(i32 %op1)
  ret i32 %op2
}
```

![image-20231206103305985](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103305985.png)



输入 `lli fun.ll; echo $?`指令进行测试，结果如下：

![image-20231206103337717](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103337717.png)

结果为220，输出正确

创建了两个块，分别为 `auto bb = BasicBlock::create(module, "entry", calleeFun);` 和 `bb = BasicBlock::create(module, "entry", mainFun); `，其分别对应.ll文件中 `callee` 函数中的标签`label_entry`和`main`函数中的标签`label_entry`。

##### 补充`if_generator.cpp`

`if_generator.cpp`代码如下：

```c++
#include "BasicBlock.h"
#include "Constant.h"
#include "Function.h"
#include "IRBuilder.h"
#include "Module.h"
#include "Type.h"

#include <iostream>
#include <memory>

#ifdef DEBUG  // 用于调试信息,大家可以在编译过程中通过" -DDEBUG"来开启这一选项
#define DEBUG_OUTPUT std::cout << __LINE__ << std::endl;  // 输出行号的简单示例
#else
#define DEBUG_OUTPUT
#endif

#define CONST_INT(num) \
    ConstantInt::get(num, module)

#define CONST_FP(num) \
    ConstantFP::get(num, module) // 得到常数值的表示,方便后面多次用到
    
int main() {
  auto module = new Module("if code");
  auto builder = new IRBuilder(nullptr, module); //该类提供了独立的接口创建各种 IR 指令
  Type *Int32Type = Type::get_int32_type(module);
  Type *FloatType = Type::get_float_type(module);
  
  // main函数
  auto mainFun = Function::create(FunctionType::get(Int32Type, {}),"main", module);
  auto bb = BasicBlock::create(module, "entry", mainFun); //创建BasicBlock
  builder->set_insert_point(bb);  //将当前插入指令点的位置设在bb
  
  auto retAlloca = builder->create_alloca(Int32Type);   // 在内存中分配返回值的位置
  
  auto aAlloca=builder->create_alloca(FloatType); //在内存中分配参数a的位置
  builder->create_store(CONST_FP(5.555), aAlloca);  //将常数5.555写入a的地址
  auto aLoad = builder->create_load(aAlloca);  //读取a
  
  auto cmp = builder->create_fcmp_gt(aLoad, CONST_FP(1.000));  //判断a是否大于1
  auto trueBB = BasicBlock::create(module, "trueBB", mainFun);    // true分支
  auto falseBB = BasicBlock::create(module, "falseBB", mainFun);  // false分支
  auto retBB = BasicBlock::create(module, "", mainFun);  // return分支,提前create,以便true分支可以br
  auto br = builder->create_cond_br(cmp, trueBB, falseBB);  // 条件BR
  
  builder->set_insert_point(trueBB);  // if true; 分支的开始需要SetInsertPoint设置
  builder->create_store(CONST_INT(233), retAlloca); //将整数233存储到返回值的地址中
  builder->create_br(retBB);  // br retBB
  
  builder->set_insert_point(falseBB);  // if false
  builder->create_store(CONST_INT(0), retAlloca);  //将整数0存储到返回值的地址中
  builder->create_br(retBB);  // br retBB

  builder->set_insert_point(retBB);  // ret分支
  auto retLoad = builder->create_load(retAlloca);  //获取返回值地址中值
  builder->create_ret(retLoad);  //创建ret指令,返回

  std::cout << module->print();
  delete module;
  return 0;
}
```



结果验证：

输入指令`make`，编译通过后输入指令`./stu_if_generator >if.ll`：

![image-20231206103451854](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103451854.png)

内容如下：

```c
define i32 @main() {
label_entry:
  %op0 = alloca i32
  %op1 = alloca float
  store float 0x40163851e0000000, float* %op1
  %op2 = load float, float* %op1
  %op3 = fcmp ugt float %op2,0x3ff0000000000000
  br i1 %op3, label %label_trueBB, label %label_falseBB
label_trueBB:                                                ; preds = %label_entry
  store i32 233, i32* %op0
  br label %label4
label_falseBB:                                                ; preds = %label_entry
  store i32 0, i32* %op0
  br label %label4
label4:                                                ; preds = %label_trueBB, %label_falseBB
  %op5 = load i32, i32* %op0
  ret i32 %op5
}
```

![image-20231206103524889](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103524889.png)

输入 `lli if.ll; echo $?`指令进行测试，结果如下：

![image-20231206103637271](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103637271.png)

结果为233，输出正确

创建了4个`basicblock`,对应关系为：

1. `auto bb = BasicBlock::create(module, "entry", mainFun);`对应`main`函数中的标签`label_entry`
2. `auto trueBB = BasicBlock::create(module, "trueBB", mainFun);`对应标签`label_trueBB`
3. `auto falseBB = BasicBlock::create(module, "falseBB", mainFun);`对应标签`label_falseBB`
4. `auto retBB = BasicBlock::create(module, "", mainFun);`对应标签`label4`



##### 补充`while_generator.cpp`

```c
#include "BasicBlock.h"
#include "Constant.h"
#include "Function.h"
#include "IRBuilder.h"
#include "Module.h"
#include "Type.h"

#include <iostream>
#include <memory>

#ifdef DEBUG  // 用于调试信息,大家可以在编译过程中通过" -DDEBUG"来开启这一选项
#define DEBUG_OUTPUT std::cout << __LINE__ << std::endl;  // 输出行号的简单示例
#else
#define DEBUG_OUTPUT
#endif

#define CONST_INT(num) \
    ConstantInt::get(num, module)

#define CONST_FP(num) \
    ConstantFP::get(num, module) // 得到常数值的表示,方便后面多次用到
    
int main() {
  auto module = new Module("while code");
  auto builder = new IRBuilder(nullptr, module); //该类提供了独立的接口创建各种 IR 指令
  Type *Int32Type = Type::get_int32_type(module);

  // main函数
  auto mainFun = Function::create(FunctionType::get(Int32Type, {}),"main", module);
  auto bb = BasicBlock::create(module, "entry", mainFun); //创建BasicBlock
  builder->set_insert_point(bb);  //将当前插入指令点的位置设在bb
  
  auto retAlloca = builder->create_alloca(Int32Type);   // 在内存中分配返回值的位置
  
  auto aAlloca=builder->create_alloca(Int32Type); //在内存中分配参数a的位置
  auto iAlloca=builder->create_alloca(Int32Type); //在内存中分配参数i的位置
  
  builder->create_store(CONST_INT(10), aAlloca);  //将10写入a的地址
  builder->create_store(CONST_INT(0), iAlloca);  //将0写入i的地址
  
  
  auto condBB = BasicBlock::create(module, "condBB", mainFun);  // cond分支,用于while循环的判断
  auto trueBB = BasicBlock::create(module, "trueBB", mainFun);    // true分支,条件为真时
  auto retBB = BasicBlock::create(module, "retBB", mainFun);  // ret分支,当循环条件不满足时
  builder->create_br(condBB); 
  
  builder->set_insert_point(condBB); // cond分支判断条件，分支的开始需要SetInsertPoint设置
  auto iLoad = builder->create_load(iAlloca);  //获取i地址中的值
  auto cmp = builder->create_icmp_lt(iLoad, CONST_INT(10));  //判断i是否小于10
  auto br = builder->create_cond_br(cmp, trueBB, retBB);  // 条件BR
   
  builder->set_insert_point(trueBB);  // if true; 
  auto aLoad = builder->create_load(aAlloca);  //从a地址读取a
  iLoad = builder->create_load(iAlloca);  //从i地址读取i
  auto add1=builder->create_iadd(iLoad,CONST_INT(1)); //进行i+1
  builder->create_store(add1, iAlloca); //把i+1的值写入i的地址
  iLoad = builder->create_load(iAlloca); //从i地址读取i
  auto add2=builder->create_iadd(iLoad,aLoad);//进行i+a
  builder->create_store(add2, aAlloca); //把i+a的值写入a的地址
  builder->create_br(condBB);  // br condBB
  
  builder->set_insert_point(retBB);  // ret分支
  aLoad = builder->create_load(aAlloca);  //从a地址读取a
  builder->create_store(aLoad,retAlloca ); //把a的值写入返回值的地址
  auto retLoad = builder->create_load(retAlloca);  //获取返回值地址中值
  builder->create_ret(retLoad);  //创建ret指令,返回

  std::cout << module->print();
  delete module;
  return 0;
}
```

结果验证：

输入指令`make`，编译通过后输入指令`./stu_while_generator >while.ll`：

![image-20231206103817014](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103817014.png)

内容如下：

```c
define i32 @main() {
label_entry:
  %op0 = alloca i32
  %op1 = alloca i32
  %op2 = alloca i32
  store i32 10, i32* %op1
  store i32 0, i32* %op2
  br label %label_condBB
label_condBB:                                                ; preds = %label_entry, %label_trueBB
  %op3 = load i32, i32* %op2
  %op4 = icmp slt i32 %op3, 10
  br i1 %op4, label %label_trueBB, label %label_retBB
label_trueBB:                                                ; preds = %label_condBB
  %op5 = load i32, i32* %op1
  %op6 = load i32, i32* %op2
  %op7 = add i32 %op6, 1
  store i32 %op7, i32* %op2
  %op8 = load i32, i32* %op2
  %op9 = add i32 %op8, %op5
  store i32 %op9, i32* %op1
  br label %label_condBB
label_retBB:                                                ; preds = %label_condBB
  %op10 = load i32, i32* %op1
  store i32 %op10, i32* %op0
  %op11 = load i32, i32* %op0
  ret i32 %op11

```



![image-20231206103943014](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103943014.png)

输入 `lli while.ll; echo $?`指令进行测试，结果如下：

![image-20231206103903913](C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206103903913.png)

结果为65，输出正确

创建了4个`basicblock`,对应关系为：

1. `auto bb = BasicBlock::create(module, "entry", mainFun);`对应`main`函数中的标签`label_entry`
2. `auto condBB = BasicBlock::create(module, "condBB", mainFun);`对应标签`label_condBB`
3. `auto trueBB = BasicBlock::create(module, "trueBB", mainFun);`对应标签`label_trueBB`
4. `auto retBB = BasicBlock::create(module, "retBB", mainFun);`对应标签`label_retBB`









## 问题2: Visitor Pattern

请指出visitor.cpp中，`treeVisitor.visit(exprRoot)`执行时，以下几个Node的遍历序列:numberA、numberB、exprC、exprD、exprE、numberF、exprRoot。  
序列请按如下格式指明：  
exprRoot->numberF->exprE->numberA->exprD



对`visitor.cpp`进行分析：

```c
class AddSubNode : public Node {  // Specific element class #1
 public:
  // Resolved at runtime, it calls the treeVisitor's overloaded function,
  // corresponding to AddSubNode.
  //在运行时解决，它调用 treeVisitor 的重载函数，对应于 AddSubNode。
  int accept(TreeVisitor& treeVisitor) override {
    return treeVisitor.visit(*this);
  }
  Node& leftNode;
  Node& rightNode;
  std::string op;
  AddSubNode(Node& left, Node& right, std::string op): leftNode(left), rightNode(right), op(op){}
};
```

对于加减结点，按照“左中右”的顺序为：左操作数（子结点），中间符号（当前结点），右操作数（子结点）。

```c
class NumberNode : public Node {  // Specific element class #2
 public:
  // Resolved at runtime, it calls the treeVisitor's overloaded function,
  // corresponding to NumberNode.
  int accept(TreeVisitor& treeVisitor) override {
    return treeVisitor.visit(*this);
  }
  int number;
  NumberNode(int number){
    this->number = number;
  }
};
```

而数字结点仅自身。

对于乘除结点，按照“左中右”的顺序为：左操作数（子结点），中间符号（当前结点），右操作数（子结点）。

**查看 visit 函数代码**

```c
int visit(AddSubNode& node) override {
    auto right = node.rightNode.accept(*this);
    auto left = node.leftNode.accept(*this);
    if (node.op == "add") {
      return left + right;
    } 
    else {
      return left - right;
    }
  }
```



**对于加减操作，先遍历右子树再遍历左子树，如果操作符是‘add’就进行加操作，否则进行减操作**



```c
int visit(MulDivNode& node) override {
    auto left = node.leftNode.accept(*this);
    auto right = node.rightNode.accept(*this);
    if (node.op == "mul") {
      return left * right;
    } 
    else {
      return left / right;
    }
  }
};
```

**对于乘除操作，先遍历左子树再遍历右子树，如果操作符是‘mul’进行乘操作，否则进行减操作。**

```c++
int visit(NumberNode& node) override {
    return node.number;
  }
```

数字节点直接返回它的值





下面查看main函数

```c
  // construct the expression nodes and the tree
  // the expression: 4 * 2 - 2 / 4 + 5
  auto numberA = NumberNode(4);
  auto numberB = NumberNode(2);
  auto exprC = MulDivNode(numberA, numberB, "mul");
  auto exprD = MulDivNode(numberB, numberA, "div");
  auto exprE = AddSubNode(exprC, exprD, "sub");
  auto numberF = NumberNode(5);
  auto exprRoot = AddSubNode(exprE, numberF, "add");
```



实际上就自底向上构建了一个树：

<img src="C:\Users\清竹\AppData\Roaming\Typora\typora-user-images\image-20231206110836784.png" alt="image-20231206110836784" style="zoom:80%;" />



然后调用visit函数：

```c
 TreeVisitorCalculator treeVisitor;
  // traverse the tree and calculate
  int result = treeVisitor.visit(exprRoot);
  std::cout << "4 * 2 - 2 / 4 + 5 evaluates: " << result << std::endl;
```



**最终的访问顺序如下：exprRoot->numberF->exprE->exprD->numberB->numberA->exprC->numberA->numberB**









## 问题3: getelementptr

请给出`IR.md`中提到的两种getelementptr用法的区别,并稍加解释:

  - `%2 = getelementptr [10 x i32], [10 x i32]* %1, i32 0, i32 %0` 
  - `%2 = getelementptr i32, i32* %1 i32 %0` 





**1.`%2 = getelementptr [10 x i32], [10 x i32]* %1, i32 0, i32 %0`**



- 概念：`getelementptr`指令用于获取数组结构的元素的地址。 它仅执行地址计算，并且不访问内存。
- 格式： `<result> = getelementptr <type>, <type>* <ptrval> [, <type> <idx>]`
- 参数解释：**第一个参数是计算基础类型，第二第三个参数表示索引开始的指针类型及指针，`[]`表示可重复参数，里面表示的数组索引的偏移类型及偏移值。**

**指针类型为[10 x i32]*，指向的数据类型为[10 x i32]，因此首先用i32 0表示偏移为0，这表示直接取第一个[10 x i32]数组，然后的i32 %0表示在第一个[10 x i32]数组内，偏移%0的元素地址。**



**2.`%2 = getelementptr i32, i32* %1 i32 %0`**

**指针类型为i32* ，即int*，%1表示的是数组的起始地址，偏移量为%0，直接取出了数组偏移%0位置的元素地址。，也是获得一个数组a[%0]位置的方法**



## 实验难点

描述在实验中遇到的问题、分析和解决方案









## 实验反馈

